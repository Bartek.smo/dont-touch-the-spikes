package main

import (
	"fmt"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
)

func run() {
	game()

}

func main() {
	pixelgl.Run(run)
}
func game() {
	cfg := pixelgl.WindowConfig{
		Title:  "DONT TOUCH THE SPIKES",
		Bounds: pixel.R(0, 0, 1000, 1000),
		VSync:  true,
	}

	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}

	//ladowanie obrazkow
	img2 := OpeningFile("birdleft.png")
	img := OpeningFile("BirdUp.png")
	img3 := OpeningFile("bubbles.png")
	img4 := OpeningFile("goralewo.png")
	img5 := OpeningFile("goraprawo.png")

	// bird obrazki zrobione
	bird := FileInit(img)
	bird2 := FileInit(img2)
	tlo := FileInit(img3)
	goralewo := FileInit(img4)
	goraprawo := FileInit(img5)

	//pozycje ptaszka i jak ma latac
	x := 500.0
	y := 500.0
	birdPosition := pixel.V(x, y)
	vector := 10.0
	velacity := 10.0

	//kolor
	imd := imdraw.New(nil)
	imd.Color = pixel.RGB(20, 0, 1)

	done := make(chan bool) // to do napisu kanal zeby czekal

	//struktury kordow z kolcami lewo prawo do tablicy
	var lewo []Spike
	lewo = KolecLewo(lewo)
	var prawo []Spike
	prawo = KolecPrawo((prawo))
	for i := 0; i < len(lewo); i++ {
		fmt.Println("lewo", i, lewo[i])
		fmt.Println("prawo", i, prawo[i])
	}
	// var hity []Spike
	trudnosc := 3
	odbicie := 0
	czyByltam := false //sprawdzanie czy ptak juz byl na sciance zeby moc usunac boczne kolce
	przyklad := 0
	czyKoniec := false //sprawdzanie czy juz

	for !win.Closed() {
		win.Clear(pixel.RGB(0, 0, 0)) // Clear the window with a black background

		tlo.Draw(win, pixel.IM.Moved(win.Bounds().Center()))

		odbicie++
		SpikeLines(imd) // gora dol kolce

		//tworzenie bocznych kolcow
		if (birdPosition.X != 0.0 && birdPosition.X != 1000.0) && czyByltam == false {
			for k := 0; k < trudnosc; k++ {
				StronaKolca(imd, birdPosition, lewo, prawo)

			}
			czyByltam = true
		} else if (birdPosition.X <= 0.0 || birdPosition.X >= 1000.0) && czyByltam == true {
			imd.Clear()

			SpikeLines(imd)

			imd.Draw(win)
			win.Update()
			czyByltam = false
			przyklad++
			if (przyklad%10 == 0) && (trudnosc != 6) {
				trudnosc++
				fmt.Println("TRUDNOSC:", trudnosc)

			}
		}
		fmt.Println(sideSpikes)

		vector = Positioning(birdPosition, vector) // kierunek poruszania po X
		fmt.Println("x:", vector)
		birdPosition = Movement(win, birdPosition, vector, velacity)
		// w ktora ma patrzec strone
		BirdPhoto(win, birdPosition, vector, bird, bird2, goralewo, goraprawo)
		///

		// uderzenie
		if CzyPrzypal(birdPosition) {
			go Przypal(win, "YOU TOUCH THE SPIKE!", done)
			break
		}
		//////////////

		if przyklad > 0 {
			for _, sideSpike := range sideSpikes {

				if CzyUderzyl(birdPosition, sideSpike) {
					go Przypal(win, "YOU TOUCH THE SPIKE!", done)
					czyKoniec = true
					fmt.Println("SPIKE", sideSpike)
				}
			}
		}
		if czyKoniec == true {

			break
		}

		imd.Draw(win)

		win.Update()
		Usuwanie(birdPosition)
		// Usuwanie poprzednich kolcow na boku
	}

	<-done // do napisu zeby czekal na niego 'przypal'

}
