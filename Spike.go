package main



import (
	
	"math/rand"
	
	"time"
	
	
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	
	
	
)
var sideSpikes []Spike
//WYGLAD
//gora dol kolce
func SpikeLines(imd *imdraw.IMDraw) {

	x1 := 0.0
	x2 := 100.0
	x3 := 50.0
	y1 := 0.0
	y2 := 0.0
	y3 := 50.0
	tx1 := 0.0
	tx2 := 100.0
	tx3 := 50.0
	ty1 := 1000.0
	ty2 := 1000.0
	ty3 := 950.0
	for i := 0; i < 10; i++ {
		Kolec(imd, x1, x2, x3, y1, y2, y3)
		Kolec(imd, tx1, tx2, tx3, ty1, ty2, ty3)
		x1 += 100
		x2 += 100
		x3 += 100
		tx1 += 100
		tx2 += 100
		tx3 += 100

	}

}
// #####Spikes#####



//kolce boczne
func KolecLewo(lewo []Spike) []Spike {
	spike := Spike{
		x1: 0.0,
		x2: 50.0,
		x3: 0.0,
		y1: 0.0,
		y2: 50,
		y3: 100,
	}
	for i := 0; i < 10; i++ {

		lewo = append(lewo, spike)
		spike.y1 += 100
		spike.y2 += 100
		spike.y3 += 100
	}
	return lewo
}
func KolecPrawo(prawo []Spike) []Spike {
	spike := Spike{
		x1: 1000.0,
		x2: 950.0,
		x3: 1000.0,
		y1: 0.0,
		y2: 50,
		y3: 100,
	}
	for i := 0; i < 10; i++ {

		prawo = append(prawo, spike)
		spike.y1 += 100
		spike.y2 += 100
		spike.y3 += 100
	}
	return prawo
}	
//rysowanie kolca
func Kolec(imd *imdraw.IMDraw, x1, x2, x3, y1, y2, y3 float64) {

	imd.Push(pixel.V(x1, y1))
	imd.Push(pixel.V(x2, y2))
	imd.Push(pixel.V(x3, y3))
	imd.Polygon(0)
	

}

//trzyma kordy kolcow lewo prawo
type Spike struct {
	x1 float64
	x2 float64
	x3 float64
	y1 float64
	y2 float64
	y3 float64
}


//ZACHOWANIE KOLCOW
// z ktorej strony ma sie pojawic Kolec ale tutaj imo trzeba gdzies go rutyne wrzucic
func StronaKolca(imd *imdraw.IMDraw, birdPosition pixel.Vec, lewo []Spike, prawo []Spike) {
	if birdPosition.X > 500.0 {
		RandomSpike(imd, lewo)
	} else if birdPosition.X < 500.0 {
		RandomSpike(imd, prawo)
	}
}

//losowanie z talicy kordow kolca po boku i wypisanie 
func RandomSpike(imd *imdraw.IMDraw, strona []Spike) Spike {
	rand.Seed(time.Now().UnixNano())

	choice := rand.Intn(len(strona))
	selectedSpike := strona[choice]

	Kolec(imd, selectedSpike.x1, selectedSpike.x2, selectedSpike.x3, selectedSpike.y1, selectedSpike.y2, selectedSpike.y3)

	sideSpikes = append(sideSpikes, selectedSpike)

	return selectedSpike
}

//usuwa z listy kolcow kolce ktore byly i zniknely
func Usuwanie(birdPosition pixel.Vec){
	if birdPosition.X >= 1000.0 || birdPosition.X <= 0.0{
		for i:=0;i<len(sideSpikes);i++{
			sideSpikes=append(sideSpikes[:i],sideSpikes[i+1:]...)
			i-=1
		}
	}

}
